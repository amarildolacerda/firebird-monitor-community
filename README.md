# firebird-monitor-community


Objetivo: Permitir fazer monitoramento de banco de dados "firebirdsql";


[configurações do servidor]
Editar o arquivo    .env

* database: utilizado para mapeamento de multiplos bancos. O padrão é  {contaid}, isto permitira utilizar tanto um endereço físico como um "alias" configurado no databases.conf

[acesso ao monitor]
* O padrão é a interface ficar disponível em  http://localhost:8886  ( na mesma porta do serviço REST)

[configurações do monitor]
Ao carregar a primeira vez será necessário adicionar o "servidor/banco de dados" na lista de bancos monitorados  (aba servidores/conexões).

Ao adicionar os parametros do banco de dados:
* nome: um de escolhar (amigável)
* conta: o alias do databases.conf
* host:  o serviço do REST ex:  http://localhost:8886
* token:  usar o icon "ferramenta" para criar um token para o banco de dados - usar login/senha do banco de usuário com permissão de acesso as tabelas de sistema;

